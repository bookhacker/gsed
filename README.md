# README #

A bunch of sed/gsed scripts (some day, hopefully).

### What is this repository for? ###

ATM I use 'em to format/search & replace dictated text into an own screenplay like format.

### How do I get set up? ###

Since it's for sed, sed needs to be installed. When you're on Mac OS, install GNU sed (gsed).

### Who do I talk to? ###

Ask the internet. There is so much help about sed to find.

### File overview

* conditionalUpcaseLastCharOfMatch.sed

This expression removes ` PLACEHOLDER ` and sets the character after ` PLACEHOLDER ` upper case depending on the character before ` PLACEHOLDER `:

`text. PLACEHOLDER text: PLACEHOLDER text! PLACEHOLDER text? PLACEHOLDER text, PLACEHOLDER text PLACEHOLDER text.` becomes `text. Text: Text! Text? Text, text text.`.