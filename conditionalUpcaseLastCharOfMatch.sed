gsed -e 's/\([.:!?]\) PLACEHOLDER \([a-zA-ZöäüÖÄÜ]\)/\1 \u\2/g' -e 's/ PLACEHOLDER / /g'

# sed version:
#
# sed -e 's/\([.:!?]\) PLACEHOLDER \([a-zA-ZöäüÖÄÜ]\)/\1 \u\2/g' -e 's/ PLACEHOLDER / /g'
#
# What it does:
#
# This expression removes ' PLACEHOLDER ' and sets the
# character *after* ' PLACEHOLDER ' upper case depending on
# the character *before* ' PLACEHOLDER '.
#
# The parts:
#
# The first part
#
# 's/\([.:!?]\) PLACEHOLDER \([a-zA-ZöäüÖÄÜ]\)/\1 \u\2/g'
#
# removes ' PLACEHOLDER ' and uppercases the next character after ' PLACEHOLDER '
#
# The second part
#
# 's/ PLACEHOLDER / /g'
#
# just replaces ' PLACEHOLDER ' with a single SPACE ' '.
#
# Example:
#
# text. PLACEHOLDER text: PLACEHOLDER text! PLACEHOLDER text? PLACEHOLDER text, PLACEHOLDER text PLACEHOLDER text.
#
# should become
#
# text. Text: Text! Text? Text, text text.
#
# Copy & paste to test (copy *without* leading '#', of course):
#
# echo 'text. PLACEHOLDER text: PLACEHOLDER text! PLACEHOLDER text? PLACEHOLDER text, PLACEHOLDER text PLACEHOLDER text.' | gsed -e 's/\([.:!?]\) PLACEHOLDER \([a-zA-ZöäüÖÄÜ]\)/\1 \u\2/g' -e 's/ PLACEHOLDER / /g'
#
# sed version:
#
# echo 'text. PLACEHOLDER text: PLACEHOLDER text! PLACEHOLDER text? PLACEHOLDER text, PLACEHOLDER text PLACEHOLDER text.' | sed -e 's/\([.:!?]\) PLACEHOLDER \([a-zA-ZöäüÖÄÜ]\)/\1 \u\2/g' -e 's/ PLACEHOLDER / /g'
#
# Troubleshooting:
#
# If it's not working:
#   - Ensure you aren't using an old version of sed.
#   - Remember to escape with double \\ on some systems.
#
# Hints:
#
# Since not all languages continue upper case after a colon
# ':', just remove it from the search pattern to deactivate
# it.
#
# Further info:
#
# The author of this script is from Germany, so only German
# letters are considered.